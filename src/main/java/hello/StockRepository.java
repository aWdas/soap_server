package hello;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;

import io.hadl.webservices.Stock;
import io.hadl.webservices.Currency;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;

@Component
public class StockRepository {
	private static final List<Stock> stocks = new ArrayList<Stock>();

	@PostConstruct
	public void initData() {
		Stock google = new Stock();
		google.setShortcut("GOOG");
		google.setHeadquarters("Mountain View");
		google.setCurrency(Currency.EUR);
		google.setValue(736);
		google.setCompany("Google");

		stocks.add(google);

		Stock apple = new Stock();
		apple.setShortcut("AAPL");
		apple.setHeadquarters("Cupertino");
		apple.setCurrency(Currency.GBP);
		apple.setValue(109);
		apple.setCompany("Apple");

		stocks.add(apple);

		Stock yahoo = new Stock();
		yahoo.setShortcut("YHOO");
		yahoo.setHeadquarters("Sunnyvale");
		yahoo.setCurrency(Currency.GBP);
		yahoo.setValue(36);
		yahoo.setCompany("Yahoo");

		stocks.add(yahoo);
	}

	public Stock findStock(String shortcut) {
		Assert.notNull(shortcut);

		Stock result = null;

		for (Stock stock : stocks) {
			if (shortcut.equals(stock.getShortcut())) {
				result = stock;
			}
		}

		return result;
	}
}
