package hello;

import io.hadl.webservices.Stock;
import io.hadl.webservices.Currency;
import io.hadl.webservices.GetStockRequest;
import io.hadl.webservices.GetStockResponse;
import io.hadl.webservices.GetStockRequest;
import io.hadl.webservices.GetStockResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

@Endpoint
public class StockEndpoint {
	private static final String NAMESPACE_URI = "http://hadl.io/webservices";

	private StockRepository countryRepository;

	@Autowired
	public StockEndpoint(StockRepository countryRepository) {
		this.countryRepository = countryRepository;
	}

	@PayloadRoot(namespace = NAMESPACE_URI, localPart = "getStockRequest")
	@ResponsePayload
	public GetStockResponse getStock(@RequestPayload GetStockRequest request) {
		GetStockResponse response = new GetStockResponse();
		Stock c = countryRepository.findStock(request.getShortcut());
		if(c == null) {
			c = new Stock();
			c.setCompany("unavailable");
			c.setCurrency(Currency.EUR);
			c.setHeadquarters("unavailable");
			c.setValue(0);
		}

		response.setStock(c);

		return response;
	}
}
